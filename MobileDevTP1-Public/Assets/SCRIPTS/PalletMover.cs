﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalletMover : ManejoPallets {

    public enum Step
    {
        first = 1, second, third
    }

    [SerializeField] private ControlDireccion inputController = null;

    private Step actualStep;

    public ManejoPallets Desde, Hasta;
    bool segundoCompleto = false;

    private void Start()
    {
        actualStep = Step.second;
    }

   private void Update() {
#if UNITY_STANDALONE

        if (inputController.Input.GetHorizontalAxis() < 0)
        {
            actualStep = Step.first;
        }

        if (inputController.Input.GetVerticalAxis() < 0)
        {
            actualStep = Step.second;
        }

        if (inputController.Input.GetHorizontalAxis() > 0)
        {
            actualStep = Step.third;
        }

        switch (actualStep)
        {
            case Step.first:
                if (!Tenencia() && Desde.Tenencia())
                {
                    FirstStep();
                }
                break;
            case Step.second:
                if (Tenencia())
                {
                    SecondStep();
                }
                break;
            case Step.third:
                if (segundoCompleto && Tenencia())
                {
                    ThirdStep();
                }
                break;
            default:
                break;
        }
#endif
        /*switch (miInput) {
            case MoveType.WASD:
                if (!Tenencia() && Desde.Tenencia() && Input.GetKeyDown(KeyCode.A)) {
                   // PrimerPaso();
                     UpdateStep(1);
                }
                if (Tenencia() && Input.GetKeyDown(KeyCode.S)) {
                     //SegundoPaso();
                     UpdateStep(2);
                 }
                if (segundoCompleto && Tenencia() && Input.GetKeyDown(KeyCode.D)) {
                     //TercerPaso();
                     UpdateStep(3);
                 }
                break;
            case MoveType.Arrows:
                if (!Tenencia() && Desde.Tenencia() && Input.GetKeyDown(KeyCode.LeftArrow)) {
                     //PrimerPaso();
                     UpdateStep(1);
                 }
                if (Tenencia() && Input.GetKeyDown(KeyCode.DownArrow)) {
                     // SegundoPaso();
                     UpdateStep(2);

                 }
                if (segundoCompleto && Tenencia() && Input.GetKeyDown(KeyCode.RightArrow)) {
                     //TercerPaso();
                     UpdateStep(3);
                 }
                break;
            default:
                break;
        }*/
    }

    public void UpdateStep(int step)
    {
        actualStep = (Step)step;

        switch (actualStep)
        {
            case Step.first:
                if (!Tenencia() && Desde.Tenencia())
                {
                    FirstStep();
                }
                break;
            case Step.second:
                if (Tenencia())
                {
                    SecondStep();
                }
                break;
            case Step.third:
                if (segundoCompleto && Tenencia())
                {
                    ThirdStep();
                }
                break;
            default:
                break;
        }
    }

    void FirstStep() {
        Desde.Give(this);
        segundoCompleto = false;
    }
    void SecondStep() {
        base.pallets[0].transform.position = transform.position;
        segundoCompleto = true;
    }
    void ThirdStep() {
        Give(Hasta);
        segundoCompleto = false;
    }

    public override void Give(ManejoPallets receptor) {
        if (Tenencia()) {
            if (receptor.Receive(pallets[0])) {
                pallets.RemoveAt(0);
            }
        }
    }

    public override bool Receive(Pallet pallet) {
        if (!Tenencia()) {
            pallet.porter = this.gameObject;
            base.Receive(pallet);
            return true;
        }
        else
            return false;
    }
}
