using UnityEngine;
using System.Collections;

public class UnloadAnimationManager : MonoBehaviour 
{
	enum Animations { Exit, Entry, Nothing }

	private Animations actualAnimation = Animations.Nothing;
	private Animation animation = null;
	private Animation doorAnimation = null;

	[SerializeField] private string entryAnimationName = "Entrada";
	[SerializeField] private string exitAnimationName = "Salida";
	[SerializeField] private string doorAnimationName = "AnimPuerta";

	public GameObject door = null;
	public UnloadController unloadController = null;

	private void Awake()
    {
		animation = GetComponent<Animation>();
		doorAnimation = door.GetComponent<Animation>();
	}

    private void Update () 
	{
		//if(Input.GetKeyDown(KeyCode.Z))
		//	Entrar();
		//if(Input.GetKeyDown(KeyCode.X))
		//	Salir();
		
		switch(actualAnimation)
		{
		case Animations.Entry:
			
			if(!animation.IsPlaying(entryAnimationName))
			{
				actualAnimation = Animations.Nothing;
				unloadController.FinAnimEntrada();
			}
			
			break;
			
		case Animations.Exit:
			
			if(!animation.IsPlaying(exitAnimationName))
			{
				actualAnimation = Animations.Nothing;
				unloadController.FinAnimSalida();
			}
			
			break;
			
		case Animations.Nothing:
			break;
		}
	}
	
	public void Entrar()
	{
		actualAnimation = Animations.Entry;
		animation.Play(entryAnimationName);

		PlayDoorAnimation(true);
	}
	
	public void Salir()
	{
		actualAnimation = Animations.Exit;
		animation.Play(exitAnimationName);

		PlayDoorAnimation(false);
	}

	private void PlayDoorAnimation(bool open)
    {
		if (door != null)
		{
			float animationStartPoint = open ? 0 : doorAnimation[doorAnimationName].length;
			int animationSpeed = open ? 1 : -1;

			doorAnimation[doorAnimationName].time = animationStartPoint;
			doorAnimation[doorAnimationName].speed = animationSpeed;
			doorAnimation.Play(doorAnimationName);
		}
	}
}
