﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum MODE { EASY = 1, NORMAL, HARD }

public class SceneManagement : MonoBehaviour
{
    #region Singletone
    private static SceneManagement instance = null;

    public static SceneManagement Instance => instance;

    private void Awake()
    {
#if UNITY_ANDROID || UNITY_IOS
		Screen.orientation = ScreenOrientation.LandscapeLeft;
#endif

        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);  
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    [SerializeField] private bool multiplayer = false;
    [SerializeField] private MODE gameMode = MODE.NORMAL;

    public bool Multiplayer => multiplayer;
    public MODE GameMode => gameMode; 

    public void SetMultiplayer(bool multiplayer)
    {
        this.multiplayer = multiplayer;
    }

    public void SetGameMode(int gameMode)
    {
        this.gameMode = (MODE)gameMode;
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("conduccion9");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void GoToCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void GoToFinalScene()
    {
        SceneManager.LoadScene("PtsFinal");
    }
}
