﻿using UnityEngine;

public class ObjectsManager : MonoBehaviour
{
    [System.Serializable]
    public struct GameModeArray
    {
        public GameObject[] toActivate;
        public GameObject[] toDeactivate;
    }

    [SerializeField] private GameModeArray easyMode;
    [SerializeField] private GameModeArray normalMode;
    [SerializeField] private GameModeArray hardMode;

    private void Start()
    {
        ActivateAndDeactivateGameModeObjects(ref easyMode);

        if ((int)SceneManagement.Instance.GameMode >= 2)
        {
            ActivateAndDeactivateGameModeObjects(ref normalMode);
        }

        if ((int)SceneManagement.Instance.GameMode == 3)
        {
            ActivateAndDeactivateGameModeObjects(ref hardMode);
        }
    }

    private void ActivateAndDeactivateGameModeObjects(ref GameModeArray gameMode)
    {
        for (int i = 0; i < gameMode.toActivate.Length; i++)
        {
            gameMode.toActivate[i].SetActive(true);
        }

        for (int i = 0; i < gameMode.toDeactivate.Length; i++)
        {
            gameMode.toDeactivate[i].SetActive(false);
        }
    }
}
