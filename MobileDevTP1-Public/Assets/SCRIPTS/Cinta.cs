using UnityEngine;
using System.Collections;

public class Cinta : ManejoPallets 
{
	private float animationTime = 0;
	private bool withPallet = false;
	private float time2 = 0;
	private Transform actualObject = null;
	private Color32 originalColorModel;

	//animacion de parpadeo
	[SerializeField] private bool playing;//lo que hace la animacion
	[SerializeField] private float range = 0.7f;
	[SerializeField] private float permanence = 0.2f;
	
	[SerializeField] private GameObject loadingBeltColor;
	[SerializeField] private Color32 blinkColor;
	[SerializeField] private float speed = 1;
	[SerializeField] private GameObject hand;
	[SerializeField] private float time = 0.5f;
	
	private void Start () 
	{
		originalColorModel = loadingBeltColor.GetComponent<Renderer>().material.color;
	}

	private void Update () 
	{
		if(playing)
		{
			animationTime += T.GetDT();
			if(animationTime > permanence)
			{
				if(loadingBeltColor.GetComponent<Renderer>().material.color == blinkColor)
				{
					animationTime = 0;
					loadingBeltColor.GetComponent<Renderer>().material.color = originalColorModel;
				}
			}
			if(animationTime > range)
			{
				if(loadingBeltColor.GetComponent<Renderer>().material.color == originalColorModel)
				{
					animationTime = 0;
					loadingBeltColor.GetComponent<Renderer>().material.color = blinkColor;
				}
			}
		}
		
		//movimiento del pallet
		for(int i = 0; i < pallets.Count; i++)
		{
			if(pallets[i].GetComponent<Renderer>().enabled)
			{
				if(!pallets[i].GetComponent<Pallet>().inSmooth)
				{
					pallets[i].GetComponent<Pallet>().enabled = false;
					pallets[i].actualTimeInLoadingBelt += T.GetDT();
					
					pallets[i].transform.position += transform.right * speed * T.GetDT();
					Vector3 vAux = pallets[i].transform.localPosition;
					vAux.y = 3.61f;//altura especifica
					pallets[i].transform.localPosition = vAux;					
					
					if(pallets[i].actualTimeInLoadingBelt >= pallets[i].totalTimeInLoadingBelt)
					{
						pallets[i].actualTimeInLoadingBelt = 0;
						actualObject.gameObject.SetActiveRecursively(false);
					}
				}
			}
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		ManejoPallets recept = other.GetComponent<ManejoPallets>();

		if(recept != null)
		{
			Give(recept);
		}
	}

	public override bool Receive(Pallet pallet)
	{
        time2 = 0;
        controller.SetPalletArrival(pallet);
        pallet.porter = gameObject;
        withPallet = true;
        actualObject = pallet.transform;
        base.Receive(pallet);
        //p.GetComponent<Pallet>().enabled = false;
        TurnOff();

        return true;
    }
	
	public void TurnOn()
	{
		playing = true;
		loadingBeltColor.GetComponent<Renderer>().material.color = originalColorModel;
	}

	public void TurnOff()
	{
		playing = false;
		withPallet = false;
		loadingBeltColor.GetComponent<Renderer>().material.color = originalColorModel;
	}
}
