using UnityEngine;

public class CollisionWithObstaclesController : MonoBehaviour 
{
	enum Collisions { All, WaitTimeToDeactivate, NoObstacles }

	private Collisions collision = Collisions.All;
	private float waitTimeLeft = 0;
	private float noCollisionTimeLeft = 0;

	[SerializeField] private float waitTime = 1;
	[SerializeField] private float noCollisionTime = 2;
    [SerializeField] private string obstacleTag = "Obstaculo";
	[SerializeField] private string truckOneName = "Camion1";

	void Start () 
	{
		Physics.IgnoreLayerCollision(8,10,false);
	}
	
	void Update () 
	{
		switch (collision)
		{
		case Collisions.WaitTimeToDeactivate:
			waitTimeLeft += T.GetDT();
			if(waitTimeLeft >= waitTime)
			{
				waitTimeLeft = 0;
				IgnoreCollisions(true);
			}
			break;
			
		case Collisions.NoObstacles:
			noCollisionTimeLeft += T.GetDT();
			if(noCollisionTimeLeft >= noCollisionTime)
			{
				noCollisionTimeLeft = 0;
				IgnoreCollisions(false);
			}
			break;

		default:
			break;
		}
	}
	
	private void OnCollisionEnter(Collision coll)
	{
		if(coll.gameObject.tag == obstacleTag)
		{
			if (collision == Collisions.All)
			{
				collision = Collisions.WaitTimeToDeactivate;
			}
		}
	}

	private void IgnoreCollisions(bool b)
	{
		int layerToIgnore = name == truckOneName ? 8 : 9;

		Physics.IgnoreLayerCollision(layerToIgnore, 10, b);

		collision = b ? Collisions.NoObstacles : Collisions.All;
	}
}
