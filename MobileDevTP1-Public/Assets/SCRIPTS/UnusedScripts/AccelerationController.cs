using UnityEngine;
using System.Collections;

public class AccelerationController : MonoBehaviour 
{
	[System.Serializable]
	public struct Pedal
    {
		public Transform transform;
		public Vector3 initialPosition;
		public float speedValue;
	}

	private float leftDifference = 0;
	private float rightDifference = 0;

	[SerializeField] private Transform rightHand = null;
	[SerializeField] private Transform leftHand = null;
	[SerializeField] private float halfHeight = 0;//valor en eje Y que calibra el 0 de cada pedal
	[SerializeField] private Transform truck = null;

	[Header("Pedals")]
	[SerializeField] private Pedal accelerationPedal;
	[SerializeField] private Pedal brakePedal;

	[Header("Sensivity configuration")]
	[SerializeField] private float accelerationSensivity = 1;
	[SerializeField] private float brakeSensivity = 1;
	[SerializeField] private float pedalSensivity = 1;

	private void Start () 
	{
		accelerationPedal.initialPosition = accelerationPedal.transform.localPosition;
		brakePedal.initialPosition = brakePedal.transform.localPosition;
	}

	private void Update () 
	{
		leftDifference = leftHand.position.y - halfHeight;
		rightDifference = rightHand.position.y - halfHeight;
		
		//acelerar
		if (rightDifference > 0)
		{
			accelerationPedal.speedValue = rightDifference * accelerationSensivity * Time.deltaTime;
			
			truck.position += accelerationPedal.speedValue * truck.forward;

			accelerationPedal.transform.localPosition = accelerationPedal.initialPosition - accelerationPedal.transform.forward * pedalSensivity * accelerationPedal.speedValue;
		}
		else
		{
			//PedalFren.localPosition = PAclPosIni;
		}
		
		//frenar
		if (leftDifference > 0)
		{
			brakePedal.speedValue = leftDifference * brakeSensivity * Time.deltaTime;
			
			truck.position -= brakePedal.speedValue * truck.forward;
			
			brakePedal.transform.localPosition = brakePedal.initialPosition - brakePedal.transform.forward * pedalSensivity * brakePedal.speedValue;
		}
		else
		{
			//PedalFren.localPosition = PFrnPosIni;
		}
	}
}
