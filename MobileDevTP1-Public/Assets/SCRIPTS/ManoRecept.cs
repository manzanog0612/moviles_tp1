using UnityEngine;
using System.Collections;

public class ManoRecept : ManejoPallets 
{
	public bool TengoPallet = false;
	
	void FixedUpdate () 
	{
		TengoPallet = Tenencia();
	}
	
	void OnTriggerEnter(Collider other)
	{
		ManejoPallets recept = other.GetComponent<ManejoPallets>();
		if(recept != null)
		{
			Give(recept);
		}
		
	}
	
	//---------------------------------------------------------//	
	
	public override bool Receive(Pallet pallet)
	{
		if(!Tenencia())
		{
			pallet.porter = this.gameObject;
			base.Receive(pallet);
			return true;
		}
		else
			return false;
	}
	
	public override void Give(ManejoPallets receptor)
	{
		//Debug.Log(gameObject.name+ " / Dar()");
		switch (receptor.tag)
		{
		case "Mano":
			if(Tenencia())
			{
				//Debug.Log(gameObject.name+ " / Dar()"+" / Tenencia=true");
				if(receptor.name == "Right Hand")
				{
					if(receptor.Receive(pallets[0]))
					{
						//Debug.Log(gameObject.name+ " / Dar()"+" / Tenencia=true"+" / receptor.Recibir(Pallets[0])=true");
						pallets.RemoveAt(0);
						//Debug.Log("pallet entregado a Mano de Mano");
					}
				}
				
			}
			break;
			
		case "Cinta":
			if(Tenencia())
			{
				if(receptor.Receive(pallets[0]))
				{
					pallets.RemoveAt(0);
					//Debug.Log("pallet entregado a Cinta de Mano");
				}
			}
			break;
			
		case "Estante":
			break;
		}
	}
}
