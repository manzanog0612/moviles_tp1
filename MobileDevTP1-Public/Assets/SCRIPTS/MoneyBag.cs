using UnityEngine;

public class MoneyBag : MonoBehaviour
{
	private Player player = null;
	private bool disapearing = false;
	private ParticleSystem particles = null;
	private Renderer rend = null;
	private Collider coll = null;

	[SerializeField] private float particlesLifeTime = 2.5f;
	[SerializeField] private Pallet.Values valueAmount = Pallet.Values.one;
	[SerializeField] private string playerTag = "";
	[SerializeField] private Texture2D inventoryImage = null;

    public Pallet.Values ValueAmount { get => valueAmount; }
    public Texture2D InventoryImage { get => inventoryImage; }

    private void Start () 
	{
		valueAmount = Pallet.Values.two;

		particles = GetComponentInChildren<ParticleSystem>();
		rend = GetComponent<Renderer>();
		coll = GetComponent<Collider>();
	}

	private void Update ()
	{
		
		if(disapearing)
		{
			particlesLifeTime -= Time.deltaTime;
			if(particlesLifeTime <= 0)
			{
				rend.enabled = true;
				coll.enabled = true;
				
				particles.GetComponent<ParticleSystem>().Stop();
				gameObject.SetActive(false);
			}
		}
		
	}

	private void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == playerTag)
		{
			player = collider.GetComponent<Player>();

			if (player.AddBag(this))
			{ 
				Disapear(); 
			}
			
		}
	}
	
	public void Disapear()
	{
		particles.Play();
		disapearing = true;
		
		rend.enabled = false;
		coll.enabled = false;
		
		if(particles != null)
		{
			particles.Play();
		}
	}
}
