using UnityEngine;
using System.Collections.Generic;

public class ManejoPallets : MonoBehaviour 
{
	protected List<Pallet> pallets = new List<Pallet>();
	public UnloadController controller;
	protected int counter = 0;
	
	public virtual bool Receive(Pallet pallet)
	{
		pallets.Add(pallet);
		pallet.Pasaje();
		return true;
	}
	
	public bool Tenencia()
	{
		
		if(pallets.Count != 0)
			return true;
		else
			return false;
		
		/*
		if(Pallets.Count > Contador)
			return true;
		else
			return false;
			*/
	}
	
	public virtual void Give(ManejoPallets receptor)
	{
		//es el encargado de decidir si le da o no la bolsa
	}
}
