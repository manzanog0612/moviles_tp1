using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour 
{
	private CheckPoint actualCP;
	private CheckPoint previousCP;

	private CarController carController = null;
	private Rigidbody body = null;
	//private VisualizationController visualization = null;
	private Player player = null;

	public float maxAngle = 90;//angulo maximo antes del cual se reinicia el camion
	int verificationByQuad = 20;
	int counter = 0;
	
	public float minRightRange = 0;
	public float maxRightRange = 0;
	
	bool ignoringCollision = false;
	public float noCollisionTime = 2;
	float time = 0;

	private void Awake()
	{
		carController = GetComponent<CarController>();
		//visualization = GetComponent<VisualizationController>();
		body = GetComponent<Rigidbody>();
		player = GetComponent<Player>();
	}

	private void Start () 
	{
		/*
		//a modo de prueba
		TiempDeNoColision = 100;
		IgnorarColision(true);
		*/
		
		//restaura las colisiones
		Physics.IgnoreLayerCollision(8,9,false);
	}

	private void Update ()
	{
		if (actualCP != null)
		{
			counter++;
			if(counter == verificationByQuad)
			{
				counter = 0;
				if(maxAngle < Quaternion.Angle(transform.rotation, actualCP.transform.rotation))
				{
					Respawnear();
				}
			}
		}
		
		if (ignoringCollision)
		{
			time += T.GetDT();
			if(time > noCollisionTime)
			{
				IgnoreCollisions(false);
			}
		}
	}
	
	public void Respawnear()
	{
		body.velocity = Vector3.zero;

		carController.SetTurn(0f);

		if (actualCP.IsEnabled())
		{
			transform.position = actualCP.transform.position;

			switch (player.actualSide)
			{
				case Player.Side.Left:
					transform.position += actualCP.transform.right * Random.Range(minRightRange * (-1), maxRightRange * (-1));
					break;
				case Player.Side.Right:
					transform.position += actualCP.transform.right * Random.Range(minRightRange, maxRightRange);
					break;
				case Player.Side.Center:
				default:
					break;
			}

			transform.forward = actualCP.transform.forward;
		}
		else if(previousCP != null)
		{
			transform.position = previousCP.transform.position;

			switch (player.actualSide)
            {
                case Player.Side.Left:
					transform.position += previousCP.transform.right * Random.Range(minRightRange * (-1), maxRightRange * (-1));
					break;
                case Player.Side.Right:
					transform.position += previousCP.transform.right * Random.Range(minRightRange, maxRightRange);
					break;
                case Player.Side.Center:
                default:
                    break;
            }
				
			transform.forward = previousCP.transform.forward;
		}
		
		IgnoreCollisions(true);
		
		//animacion de resp
		
	}
	
	//public void Respawnear(Vector3 pos)
	//{
	//	body.velocity = Vector3.zero;
	//
	//	carController.SetTurn(0f);
	//
	//	transform.position = pos;
	//	
	//	IgnoreCollisions(true);
	//}
	
	public void Respawnear(Vector3 pos, Vector3 dir)
	{
		body.velocity = Vector3.zero;

		carController.SetTurn(0f);

		transform.position = pos;
		transform.forward = dir;
		
		IgnoreCollisions(true);
	}
	
	public void AddCP(CheckPoint cp)
	{
		if(cp != actualCP)
		{
			previousCP = actualCP;
			actualCP = cp;
		}
	}
	
	void IgnoreCollisions(bool b)
	{
		//no contempla si los dos camiones respawnean relativamente cerca en el espacio 
		//temporal y uno de ellos va contra el otro, 
		//justo el segundo cancela las colisiones e inmediatamente el 1º las reactiva, 
		//entonces colisionan, pero es dificil que suceda. 
		
		Physics.IgnoreLayerCollision(8,9,b);
		ignoringCollision = b;	
		time = 0;
	}
}
