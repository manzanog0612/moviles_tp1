using UnityEngine;
using System.Collections;

public class EstanteLlegada : ManejoPallets
{
	public GameObject Mano;
	public CalibrationController ContrCalib;
	
	public override bool Receive(Pallet p)
	{
        p.porter = this.gameObject;
        base.Receive(p);
        ContrCalib.EndTutorial();

        return true;
    }
}
