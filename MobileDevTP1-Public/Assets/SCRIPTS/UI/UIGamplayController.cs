﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class UIGamplayController : MonoBehaviour, IObserver
{
	[SerializeField] private Player player = null;
	private IEnumerator blinkInst = null;
	private IEnumerator updateBonusInst = null;
	private Canvas canvas = null;
	private bool wheelEnabled = true;

	[Header("Cameras")]
	[SerializeField] private Camera calibrationCam = null;
	[SerializeField] private Camera drivingCam = null;
	[SerializeField] private Camera unloadCam = null;

	[Header("UI")]
	//[SerializeField] 
	[SerializeField] private Image[] inventoryImages = null;
	[SerializeField] private GameObject[] stepButtons = null;
	[SerializeField] private TextMeshProUGUI bonusText = null;
	[SerializeField] private GameObject wheel = null;
	[SerializeField] private GameObject bonus = null;
	[SerializeField] private Slider bonusSlider = null;
	[SerializeField] private GameObject moneyPanel = null;
	[SerializeField] private TextMeshProUGUI moneyText = null;

	[Header("Inventory visual configuration")]
	[SerializeField] private float blink = 0.8f;
	[SerializeField] private float blinkTime = 0;
	[SerializeField] private bool firstImage = true;

	private void Start()
	{
		TurnOffAllInventoryImages();

		SetButtonsActive(true);
		bonusSlider = bonus.GetComponentInChildren<Slider>();

		canvas = GetComponent<Canvas>();

#if UNITY_STANDALONE
		wheelEnabled = false;
#elif UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
		wheelEnabled = true;
#endif
	}

	public void ObserverUpdate(ISubject subject)
	{
		switch (subject.GetState())
		{
			case Player.States.Driving:

				canvas.worldCamera = drivingCam;

				if (stepButtons[0].activeSelf)
				{
					SetButtonsActive(false);
					bonusText.enabled = false;
				}

				if (!wheel.activeSelf && wheelEnabled)
				{
					wheel.SetActive(true);
				}

				if (!moneyPanel.activeSelf)
                {
					moneyPanel.SetActive(true);
				}

				UpdateUI();

				UpdateMoney();

				if (updateBonusInst != null)
				{
					StopCoroutine(updateBonusInst);
					updateBonusInst = null;
				}

				break;
			case Player.States.InCalibration:
			case Player.States.InTutorial:

				canvas.worldCamera = calibrationCam;

				if (wheel.activeSelf)
				{
					wheel.SetActive(false);
				}

				if (bonusText.enabled)
                {
					bonusText.enabled = false;
				}

				if (blinkInst != null)
				{
					StopCoroutine(blinkInst);
					blinkInst = null;
				}

				if (updateBonusInst != null)
				{
					StopCoroutine(updateBonusInst);
					updateBonusInst = null;
				}

				if (moneyPanel.activeSelf)
				{
					moneyPanel.SetActive(false);
				}

				break;
			case Player.States.OnUnload:

				canvas.worldCamera = unloadCam;

				if (!stepButtons[0].activeSelf)
				{
					SetButtonsActive(true);
				}

				if (wheel.activeSelf)
				{
					wheel.SetActive(false);
				}

				if (updateBonusInst == null)
				{
					updateBonusInst = UpdateBonus();
					StartCoroutine(updateBonusInst);
				}

				if (moneyPanel.activeSelf)
				{
					moneyPanel.SetActive(false);
				}

				UpdateUI();

				break;
			case Player.States.Off:

				if (blinkInst != null)
				{
					StopCoroutine(blinkInst);
					blinkInst = null;
				}

				if (updateBonusInst != null)
				{
					StopCoroutine(updateBonusInst);
					updateBonusInst = null;
				}

				if (moneyPanel.activeSelf)
				{
					moneyPanel.SetActive(false);
				}

				TurnOffVisualization();
				break;
			default:
				break;
		}
	}

	private void TurnOffVisualization()
	{
		calibrationCam.gameObject.SetActive(false);
		drivingCam.gameObject.SetActive(false);
		unloadCam.gameObject.SetActive(false);
		canvas.gameObject.SetActive(false);
	}

	private void TurnOffAllInventoryImages()
	{
		for (int i = 0; i < inventoryImages.Length; i++)
		{
			if (inventoryImages[i].enabled)
				inventoryImages[i].enabled = false;
		}
	}

	private void SetButtonsActive(bool active)
	{
		for (int i = 0; i < stepButtons.Length; i++)
		{
			stepButtons[i].SetActive(active);
		}
	}

	private void UpdateUI()
	{
		int counter = 0;
		for (int i = 0; i < 3; i++)
		{
			if (player.bags[i] != null)
				counter++;
		}

		TurnOffAllInventoryImages();

		if (counter < 3)
		{
			inventoryImages[counter].enabled = true;

			if (blinkInst != null)
			{
				StopCoroutine(blinkInst);
				blinkInst = null;
			}
		}
		else if (blinkInst == null)
		{
			blinkInst = Blink();

			StartCoroutine(blinkInst);
		}
	}

	private void UpdateMoney()
    {
		moneyText.text = "$" + player.money.ToString();
	}

	private IEnumerator Blink()
    {
		while (true)
        {
			blinkTime += T.GetDT();

			if (blinkTime >= blink)
			{
				blinkTime = 0;
				if (firstImage)
					firstImage = false;
				else
					firstImage = true;
			}

			if (firstImage)
			{
				inventoryImages[3].enabled = true;
			}
			else
			{
				inventoryImages[4].enabled = true;
			}

			yield return null;
		}
    }

	private IEnumerator UpdateBonus()
    {
		while (true)
        {
			if (player.unloadController.PEnMov != null)
			{
				if (!bonus.activeSelf)
				{
					bonus.SetActive(true);
				}

				bonusSlider.value = (player.unloadController.Bonus / (int)Pallet.Values.two);

				bonusText.text = "$" + player.unloadController.Bonus.ToString();
				bonusText.enabled = true;
			}
			else
			{
				bonus.SetActive(false);
				bonusSlider.value = 1;
				bonusText.enabled = false;
			}

			yield return null;
		}
    }

	public string PrepareNumbers(int money)
	{
		string startMoney = money.ToString();
		string res = "";

		if (money < 1) //without money
		{
			res = "";
		}
		else if (startMoney.Length == 6) //houndred thousands
		{
			for (int i = 0; i < startMoney.Length; i++)
			{
				res += startMoney[i];

				if (i == 2)
				{
					res += ".";
				}
			}
		}
		else if (startMoney.Length == 7) //millions
		{
			for (int i = 0; i < startMoney.Length; i++)
			{
				res += startMoney[i];

				if (i == 0 || i == 3)
				{
					res += ".";
				}
			}
		}

		return res;
	}
}
