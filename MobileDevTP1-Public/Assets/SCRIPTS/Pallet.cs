using UnityEngine;
using System.Collections;

public class Pallet : MonoBehaviour 
{
	public Values value = Values.two;
	public float time;
	public GameObject receiverLoadingBelt = null;
	public GameObject porter = null;
	public float totalTimeInLoadingBelt = 1.5f;
	public float actualTimeInLoadingBelt = 0;

	public enum Values {one = 100000, two = 250000, three = 500000}
	
	public float smoothTime = 0.3f;
	float actualSmoothTime = 0;
	public bool inSmooth = false;
	
	void Start()
	{
		time = 3;
		Pasaje();
	}

	void LateUpdate () 
	{
		if(porter != null)
		{
			if(inSmooth)
			{
				actualSmoothTime += T.GetDT();
				if(actualSmoothTime >= smoothTime)
				{
					inSmooth = false;
					actualSmoothTime = 0;
				}
				else
				{					
					if(porter.GetComponent<ManoRecept>() != null)
						transform.position = porter.transform.position - Vector3.up * 1.2f;
					else
						transform.position = Vector3.Lerp(transform.position, porter.transform.position, T.GetDT() * 10);
				}
				
			}
			else
			{				
				if(porter.GetComponent<ManoRecept>() != null)
					transform.position = porter.transform.position - Vector3.up * 1.2f;
				else
					transform.position = porter.transform.position;
					
			}
		}
			
	}
	
	//----------------------------------------------//
	
	public float GetBonus(ref float bonusTime)
	{
		float value = 0;
        switch (this.value)
        {
            case Values.one:
				value = 100000;
				break;
            case Values.two:
				value = 250000;
				break;
            case Values.three:
				value = 500000;
				break;
            default:
				value = 100000;
				break;
        }

        if(bonusTime > 0)
		{
			bonusTime -= T.GetDT();
			return bonusTime * value / time;
		}
		else
		{
			return 0;
		}
	}
	
	public void Pasaje()
	{
		inSmooth = true;
		actualSmoothTime = 0;
	}
}
