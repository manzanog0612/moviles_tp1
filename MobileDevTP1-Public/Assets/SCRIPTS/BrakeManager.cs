using UnityEngine;
using System.Collections;

public class BrakeManager : MonoBehaviour 
{
	//public float entrySpeed = 0;
	[SerializeField] private string depositTag = "Deposito";
	[SerializeField] private bool braking = false;

	private ControlDireccion directionController = null;
	private CarController carController = null;
	private Rigidbody body = null;
	private float stopedTime = 0.5f;
	private float time = 0f;
	private Vector3 destiny = Vector3.zero;
	//private bool slowingDown = false;

	//private float DagMax = 15f;
	//private float DagIni = 1f;
	//private int Contador = 0;
	//private int CantMensajes = 10;

	private void Awake()
    {
		carController = GetComponent<CarController>();
		directionController = GetComponent<ControlDireccion>();
		body = GetComponent<Rigidbody>();
	}

	private void Start () 
	{
		//RestoreVelocity();
		Brake();
	}

	private void FixedUpdate ()
	{
		if(braking)
		{
			time += T.GetFDT();
			//if(time >= (stopedTime / CantMensajes) * Contador)
			//{
			//	Contador++;
				//gameObject.SendMessage("SetDragZ", (float) (DagMax / CantMensajes) * Contador);
			//}
			if(time >= stopedTime)
			{
				//termino de frenar, que haga lo que quiera
			}
		}
	}

	private void OnTriggerEnter(Collider other) 
	{
		if(other.tag == depositTag)
		{
			Deposit deposit = other.GetComponent<Deposit>();

			if(deposit.Empty)
			{	
				if(GetComponent<Player>().HasBags())
				{
					deposit.Entrar(GetComponent<Player>());
					destiny = other.transform.position;
					transform.forward = destiny - transform.position;
					Brake();
				}				
			}
		}
	}
	
	public void Brake()
	{
		directionController.enabled = false;
		carController.SetAcceleration(0f);
		body.velocity = Vector3.zero;
		
		braking = true;
		
		time = 0;
	}
	
	public void RestoreVelocity()
	{
		directionController.enabled = true;
		carController.SetAcceleration(1f);
		braking = false;
		time = 0;
	}
}
