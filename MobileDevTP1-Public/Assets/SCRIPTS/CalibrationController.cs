using UnityEngine;

public class CalibrationController : MonoBehaviour
{
	public enum State { Calibrating, Tutorial, Finished }

	private float time = 0;

	[SerializeField] private Player player = null;
	[SerializeField] private float calibrationWaitTime = 3;
	[SerializeField] private State actualState = State.Calibrating;
	[SerializeField] private ManejoPallets departure;
	[SerializeField] private ManejoPallets arrival;
	[SerializeField] private Pallet pallet;
	[SerializeField] private ManejoPallets palletsMover;
	[SerializeField] private GameManager gameManager = null;

    public State ActualState { get => actualState; }

    private void Start () 
	{
        palletsMover.enabled = false;
        player.calibrationController = this;
		
		pallet.receiverLoadingBelt = arrival.gameObject;
		departure.Receive(pallet);
		
		SetActiveComponents(false);
	}

	private void Update ()
	{
		if(actualState == State.Tutorial)
		{
			if(time < calibrationWaitTime)
			{
				time += T.GetDT();
				if(time > calibrationWaitTime)
				{
					 SetActiveComponents(true);
				}
			}
		}
	}
	
	public void InitTesting()
	{
		actualState = State.Tutorial;
        palletsMover.enabled = true;
    }
	
	public void EndTutorial()
	{
		actualState = State.Finished;
        palletsMover.enabled = false;
        gameManager.EndCalibration(player.idPlayer);
	}

	private void SetActiveComponents(bool enabled)
	{
		ActivateComponent(departure, enabled);
		ActivateComponent(arrival, enabled);

		pallet.GetComponent<Renderer>().enabled = enabled;
	}

	private void ActivateComponent(ManejoPallets palletManager, bool enabled)
    {
		Renderer renderer = palletManager.GetComponent<Renderer>();
		Collider collider = palletManager.GetComponent<Collider>();

		if (renderer != null)
		{
			renderer.enabled = enabled; 
		}

		collider.enabled = enabled;
	}
}
