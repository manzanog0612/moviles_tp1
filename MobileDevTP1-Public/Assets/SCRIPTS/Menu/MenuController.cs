﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Image[] singlePlayerImages = null;
    [SerializeField] private Image[] multiplayerPlayerImages = null;
    [SerializeField] private GameObject[] gameModePanel = null;

    public void SetMultiplayer()
    {
        singlePlayerImages[0].enabled = false;
        singlePlayerImages[1].enabled = false;
        multiplayerPlayerImages[0].enabled = true;
        multiplayerPlayerImages[1].enabled = true;

        SceneManagement.Instance.SetMultiplayer(true);
    }

    public void SetSingleplayer()
    {
        singlePlayerImages[0].enabled = true;
        singlePlayerImages[1].enabled = true;
        multiplayerPlayerImages[0].enabled = false;
        multiplayerPlayerImages[1].enabled = false;

        SceneManagement.Instance.SetMultiplayer(false);
    }

    public void GoToGame()
    {
        SceneManagement.Instance.GoToGame();
    }

    public void GoToCredits()
    {
        SceneManagement.Instance.GoToCredits();
    }

    public void GoToMenu()
    {
        SceneManagement.Instance.GoToMenu();
    }

    public void SetGameMode(int index)
    {
        for (int i = 0; i < gameModePanel.Length; i++)
        {
            gameModePanel[i].SetActive(false);
        }

        gameModePanel[index].SetActive(true);

        SceneManagement.Instance.SetGameMode(index + 1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
