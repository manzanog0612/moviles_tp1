﻿public abstract class TruckInputManager
{
    public abstract float GetHorizontalAxis();
    public abstract float GetVerticalAxis();
}
