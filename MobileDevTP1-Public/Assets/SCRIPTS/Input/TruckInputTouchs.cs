﻿using UnityEngine;
using UnityEngine.InputSystem;

public class TruckInputTouchs : TruckInputManager
{
    private PlayerInput playerInput = null;

    public TruckInputTouchs(PlayerInput playerInput)
    {
        this.playerInput = playerInput;
    }

    private Vector2 GetInput()
    {
        return playerInput.actions["Move"].ReadValue<Vector2>();
    }

    public override float GetHorizontalAxis()
    {
        Vector2 input = playerInput.actions["Move"].ReadValue<Vector2>();

        return GetInput().x;
    }

    public override float GetVerticalAxis()
    {
        Vector2 input = GetInput();

        if (input.y < 0)
        {
            input.y = 0;
        }

        return input.y;
    }
}
