﻿using UnityEngine;

public class TruckInputKeys : TruckInputManager
{
    public enum Keys {WASD, Arrows};

    private bool playerOneKeys = true;

    public TruckInputKeys(bool _playerOneKeys)
    {
        playerOneKeys = _playerOneKeys;
    }

    public override float GetHorizontalAxis()
    {
        string horizontal = playerOneKeys ? "Horizontal1" : "Horizontal2";

        return Input.GetAxis(horizontal);
    }

    public override float GetVerticalAxis()
    {
        string vertical = playerOneKeys ? "Vertical1" : "Vertical2";

        return Input.GetAxis(vertical);
    }
}
