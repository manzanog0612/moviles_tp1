using UnityEngine;
using System.Collections;

public class Inventario : MonoBehaviour 
{
	private Player player = null;
	
	[SerializeField] private Vector2 backgroundPosition = Vector2.zero;
	[SerializeField] private Vector2 backgroundScale = Vector2.zero;
	[SerializeField] private Vector2 firstScaleSlot = Vector2.zero;
	[SerializeField] private Vector2 firstPositionSlot = Vector2.zero;
	[SerializeField] private Vector2 separation = Vector2.zero;
	[SerializeField] private int row = 0;
	[SerializeField] private int column = 0;
	[SerializeField] private Texture2D emptyTexture;//lo que aparece si no hay ninguna bolsa
	[SerializeField] private Texture2D backgroundText;
	[SerializeField] private GUISkin skin;

	private Rect rectangle;

	private void Start () 
	{
		player = GetComponent<Player>();
	}

	private void OnGUI()
	{
		switch(player.actualState)
		{
		case Player.States.Driving:
			GUI.skin = skin;

			float screenH = Screen.height / 100;
			float screenW = Screen.width / 100;

			//background
			skin.box.normal.background = backgroundText;
			rectangle.width = backgroundScale.x * screenW;
			rectangle.height = backgroundScale.y * screenH;
			rectangle.x = backgroundPosition.x * screenW;
			rectangle.y = backgroundPosition.y * screenH;
			GUI.Box(rectangle,"");
			
			//bags
			rectangle.width = firstScaleSlot.x * screenW;
			rectangle.height = firstScaleSlot.y * screenH;
			int contador = 0;
			for(int j = 0; j < row; j++)
			{
				for(int i = 0; i < column; i++)
				{
					rectangle.x = firstPositionSlot.x * screenW + separation.x * i * screenW;
					rectangle.y = firstPositionSlot.y * screenH + separation.y * j * screenH;
					
					if(contador < player.bags.Length )//&& Pj.Bolasas[contador] != null)
					{
						if(player.bags[contador]!=null)
							skin.box.normal.background = player.bags[contador].InventoryImage;
						else
							skin.box.normal.background = emptyTexture;
					
					}
					else
					{
						skin.box.normal.background = emptyTexture;
					}
					GUI.Box(rectangle,"");
					
					contador++;
				}
			}
			GUI.skin = null;
				break;
		}
		
		
	}
}
