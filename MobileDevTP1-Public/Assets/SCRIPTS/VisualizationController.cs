using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VisualizationController : MonoBehaviour, IObserver
{
	private bool alreadySet = false;

	[Header("Cameras")]
	[SerializeField] private Camera calibrationCam = null;
	[SerializeField] private Camera drivingCam = null;
	[SerializeField] private Camera unloadCam = null;

	//[System.Serializable]
	//public struct VisualObject
	//{
	//	public Vector2[] position;
	//	public Vector2 scale;
	//	public float scalef;
	//	public GUISkin skin;
	//	public Texture2D texture;
	//}


	//public enum Side { Left, Right, Center }
	//public enum VisualState { ToUnload, TutorialDriving, Calibrating}

	//private ControlDireccion directionController = null;
	//private Player player = null;
	//private Rect drawRectangle;
	//private float tutorialTime = 0;
	//private int inCourse = -1;

	public void ObserverUpdate(ISubject subject)
	{
		if (alreadySet) return;

		Player.Side side = subject.GetSide();

		Rect r = new Rect();

		r.width = drivingCam.rect.width;
		r.height = drivingCam.rect.height;
		r.y = drivingCam.rect.y;

		if (side == Player.Side.Center)
		{
			r.width = drivingCam.rect.width * 2;
		}

		switch (side)
		{
			case Player.Side.Right:
				r.x = 0.5f;
				break;
			case Player.Side.Left:
			case Player.Side.Center:
				r.x = 0;
				break;
		}

		calibrationCam.rect = r;
		drivingCam.rect = r;
		unloadCam.rect = r;

		if (side != Player.Side.Right)
		{
			roof.GetComponent<Renderer>().material.mainTexture = textNumber1;
		}
		else
		{
			roof.GetComponent<Renderer>().material.mainTexture = textNumber2;
		}

		alreadySet = true;
	}

	//[Header("UI")]
	//[SerializeField] private Canvas canvas = null;
	//[SerializeField] private Image[] inventoryImages = null;
	//[SerializeField] private GameObject[] stepButtons = null;
	//[SerializeField] private TextMeshProUGUI bonusText = null;
	//[SerializeField] private GameObject wheel = null;
	//[SerializeField] private GameObject bonus = null;
	//[SerializeField] private Slider bonusSlider = null;
	//
	//[Header("Inventory visual configuration")]
	//[SerializeField] private float blink = 0.8f;
	//[SerializeField] private float blinkTime = 0;
	//[SerializeField] private bool firstImage = true;
	//
	//[Header("Calibration and tutorial visual configuration")]
	//[SerializeField] private float range = 0.8f;//tiempo de cada cuanto cambia de imagen
	//
	[Header("Visual configuration")]
	[SerializeField] private Texture2D textNumber1 = null;//NUMERO DEL JUGADOR
	[SerializeField] private Texture2D textNumber2 = null;
	[SerializeField] private GameObject roof = null;
	//
	//public Side actualSide = Side.Left;

    /*private void Start () 
	{
		TurnOffAllInventoryImages();
		tutorialTime = range;
		directionController = GetComponent<ControlDireccion>();
		player = GetComponent<Player>();

		TurnOffAllInventoryImages();
		SetButtonsActive(true);

		bonusSlider = bonus.GetComponentInChildren<Slider>();
	}*/

    /*private void Update()
    {
		switch (player.actualState)
		{
			case Player.States.Driving:

				canvas.worldCamera = drivingCam;

				if (stepButtons[0].activeSelf)
                {
					SetButtonsActive(false);
					bonusText.enabled = false;
				}
				if (!wheel.activeSelf)
                {
					wheel.SetActive(true);
				}
				
				UpdateUI();

				break;
			case Player.States.InCalibration:
			case Player.States.InTutorial:

				canvas.worldCamera = calibrationCam;

				if (wheel.activeSelf)
				{
					wheel.SetActive(false);
					bonusText.enabled = false;
				}

				break;
			case Player.States.OnUnload:

				canvas.worldCamera = unloadCam;

				if (!stepButtons[0].activeSelf)
				{
					SetButtonsActive(true);
				}

				if (wheel.activeSelf)
				{
					wheel.SetActive(false);
				}

				SetBonus();

				UpdateUI();
				
				break;
			default:
				break;
		}
	}*/

    /*public void TurnOffVisualization()
    {
		calibrationCam.gameObject.SetActive(false);
		drivingCam.gameObject.SetActive(false);
		unloadCam.gameObject.SetActive(false);
		canvas.gameObject.SetActive(false);
	}*/

	/*private void TurnOffAllInventoryImages()
    {
		for (int i = 0; i < inventoryImages.Length; i++)
		{
			if (inventoryImages[i].enabled)
				inventoryImages[i].enabled = false;
		}
	}

	private void SetButtonsActive(bool active)
    {
        for (int i = 0; i < stepButtons.Length; i++)
        {
			stepButtons[i].SetActive(active);
		}
	}

	private void UpdateUI()
    {
		int counter = 0;
		for (int i = 0; i < 3; i++)
		{
			if (player.bags[i] != null)
				counter++;
		}

		TurnOffAllInventoryImages();

		if (counter < 3)
		{
			inventoryImages[counter].enabled = true;
		}
		else
		{
			blinkTime += T.GetDT();

			if (blinkTime >= blink)
			{
				blinkTime = 0;
				if (firstImage)
					firstImage = false;
				else
					firstImage = true;
			}

			if (firstImage)
			{
				inventoryImages[3].enabled = true;
			}
			else
			{
				inventoryImages[4].enabled = true;
			}
		}
	}

	public void ChangeToVisualState(VisualState visualState)
    {
		calibrationCam.enabled = false;
		drivingCam.enabled = false;
		unloadCam.enabled = false;

        switch (visualState)
        {
            case VisualState.ToUnload:
				unloadCam.enabled = true;
				break;
            case VisualState.TutorialDriving:
				drivingCam.enabled = true;
				break;
            case VisualState.Calibrating:
				calibrationCam.enabled = true;
				break;
            default:
                break;
        }
    }*/
	
	/*public void SetSide(Player.Side side)
	{
		//actualSide = side;
		
		Rect r = new Rect();

		r.width = drivingCam.rect.width;
		r.height = drivingCam.rect.height;
		r.y = drivingCam.rect.y;
		
		if (side == Player.Side.Center)
        {
			r.width = drivingCam.rect.width * 2;
		}

		switch (side)
		{
		case Player.Side.Right:
			r.x = 0.5f;
			break;
		case Player.Side.Left:
		case Player.Side.Center:
			r.x = 0;
			break;
		}
		
		calibrationCam.rect = r;
		drivingCam.rect = r;
		unloadCam.rect = r;
		
		if(side != Player.Side.Right)
		{
			roof.GetComponent<Renderer>().material.mainTexture = textNumber1;
		}
		else
		{
			roof.GetComponent<Renderer>().material.mainTexture = textNumber2;
		}
	}*/

	/*private void SetBonus()
	{
		if(player.unloadController.PEnMov != null)
		{
			if (!bonus.activeSelf)
            {
				bonus.SetActive(true);
			}

			bonusSlider.value = (player.unloadController.Bonus / (int)Pallet.Values.two);

			bonusText.text = "$" + player.unloadController.Bonus.ToString("0");
			bonusText.enabled = true;
		}
        else
        {
			bonus.SetActive(false);
			bonusSlider.value = 1;
			bonusText.enabled = false;
		}
	}
	
	public string PrepareNumbers(int money)
	{
		string startMoney = money.ToString();
		string res = "";
		
		if(money < 1) //without money
		{
			res = "";
		}
		else if(startMoney.Length == 6) //houndred thousands
		{
			for(int i = 0; i < startMoney.Length; i++)
			{
				res += startMoney[i];
				
				if(i == 2)
				{
					res += ".";
				}
			}
		}
		else if(startMoney.Length == 7) //millions
		{
			for(int i = 0; i < startMoney.Length; i++)
			{
				res += startMoney[i];
				
				if(i == 0 || i == 3)
				{
					res += ".";
				}
			}
		}
		
		return res;
	}*/
}
