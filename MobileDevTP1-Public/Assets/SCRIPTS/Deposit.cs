using UnityEngine;
using System.Collections;

public class Deposit : MonoBehaviour 
{
	private UnloadController controller1 = null;
	private UnloadController controller2 = null;

	[SerializeField] private bool empty = true;

    [Header("Players configuration")]
	[SerializeField] private Player actualPlayer = null;
	[SerializeField] private Collider[] playerColliders = null;
	[SerializeField] private string playerTag = "Player";
	
    public bool Empty { get => empty; }

    private void Start () 
	{		
		Physics.IgnoreLayerCollision(8,9,false);
	}

	private void Update () 
	{
		if(!empty)
		{
			actualPlayer.transform.position = transform.position;
			actualPlayer.transform.forward = transform.forward;
		}
	}
	
	public void LetGo()
	{
		actualPlayer.EmptyInventory();
		actualPlayer.GetComponent<BrakeManager>().RestoreVelocity();
		actualPlayer.GetComponent<Respawn>().Respawnear(transform.position,transform.forward);
		
		actualPlayer.GetComponent<Rigidbody>().useGravity = true;
		for(int i = 0; i < playerColliders.Length; i++)
			playerColliders[i].enabled = true;
		
		Physics.IgnoreLayerCollision(8,9,false);
		
		actualPlayer = null;
		empty = true;
		
	
	}

	public void SetControllers(UnloadController controller_1, UnloadController controller_2)
    {
		controller1 = controller_1;
		controller2 = controller_2;
	}
	
	public void Entrar(Player player)
	{
		if(player.HasBags())
		{
			
			actualPlayer = player;
			
			playerColliders = actualPlayer.GetComponentsInChildren<Collider>();
			for(int i = 0; i < playerColliders.Length; i++)
				playerColliders[i].enabled = false;
			actualPlayer.GetComponent<Rigidbody>().useGravity = false;
			
			actualPlayer.transform.position = transform.position;
			actualPlayer.transform.forward = transform.forward;
			
			empty = false;
			
			Physics.IgnoreLayerCollision(8,9,true);
			
			Entro();
		}
	}
	
	public void Entro()
	{		
		if(actualPlayer.idPlayer == 0)
			controller1.Activar(this);
		else
			controller2.Activar(this);
	}
}
