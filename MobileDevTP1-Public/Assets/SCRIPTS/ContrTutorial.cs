using UnityEngine;
using System.Collections;

public class ContrTutorial : MonoBehaviour 
{
	public Player Pj;
	public float TiempTuto = 15;
	public float Tempo = 0;
	
	public bool Finalizado = false;
	bool Iniciado = false;

	void Start () 
	{		
		Pj.tutorialController = this;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Player>() == Pj)
			Finalizar();
	}
	
	public void Iniciar()
	{
		Pj.GetComponent<BrakeManager>().RestoreVelocity();
		Iniciado = true;
	}
	
	public void Finalizar()
	{
		Finalizado = true;
		GameManager.Instance.EndTutorial(Pj.idPlayer);
		Pj.GetComponent<BrakeManager>().Brake();
		Pj.GetComponent<Rigidbody>().velocity = Vector3.zero;
		Pj.EmptyInventory();
	}
}
