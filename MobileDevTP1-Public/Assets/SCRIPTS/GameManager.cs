using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager instance = null;

    void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		}

		instance = this;
	}

	public static GameManager Instance => instance; 
	#endregion

	public enum GameState { Calibrating, Playing, Ended }

	[Header("Gameplay configuration")]
	public float gameTime = 60;
	public GameState actualState = GameState.Calibrating;

	[Header("Players")]
	public PlayerInfo player1Info = null;
	public PlayerInfo player2Info = null;
	public Player player1;
	public Player player2;

	[SerializeField] private UnloadController unloadController1 = null;
	[SerializeField] private UnloadController unloadController2 = null;
	[SerializeField] private Deposit[] deposits = null;

	[SerializeField] private Canvas[] observer = null;
	[SerializeField] private VisualizationController[] observer2 = null;

	[SerializeField] private CarCamera player2DivingCam = null;
	
	public Vector3[] skeletonRacePosition;
	
	bool countingBack = true;
	public Rect ConteoPosEsc;
	public float countToStart = 3;
	public GUISkin GS_ConteoInicio;
	
	public Rect TiempoGUI = new Rect();
	public GUISkin GS_TiempoGUI;
	Rect R = new Rect();
	
	public float waitTimeForShowingPoints = 3;
	
	public Vector3[]truckRacePosition = new Vector3[3]; 
	public Vector3 PosCamion1Tuto = Vector3.zero;
	public Vector3 PosCamion2Tuto = Vector3.zero;
	
	public GameObject[] ObjsCalibracion1;
	public GameObject[] ObjsCalibracion2;
	public GameObject[] ObjsTuto1;
	public GameObject[] ObjsTuto2;
	public GameObject[] ObjsCarrera;

    void Start()
	{
		InitializeCalibration();

        for (int i = 0; i < deposits.Length; i++)
        {
			deposits[i].SetControllers(unloadController1, unloadController2);
		}

		//observer[0]

		player1.Attach(observer[0].GetComponent<IObserver>());		
		player1.Attach(observer2[0].GetComponent<IObserver>());
		player2.Attach(observer[1].GetComponent<IObserver>());
		

		if (!SceneManagement.Instance.Multiplayer)
		{
			DeactivatePlayer2();
		}
        else
        {
			player2.Attach(observer2[1].GetComponent<IObserver>());
		}
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		
		switch (actualState)
		{
		case GameState.Calibrating:
			
			//SKIP TUTORIAL
			if(Input.GetKey(KeyCode.Mouse0) &&  Input.GetKey(KeyCode.Keypad0))
			{
				if (SceneManagement.Instance.Multiplayer)
				{
					if (player1Info != null && player2Info != null)
					{
						EndCalibration(0);
						EndTutorial(0);

						EndCalibration(1);
						EndTutorial(1);
					}
				}
                else
                {
					if (player1Info != null)
					{
						EndCalibration(0);
						EndTutorial(0);
					}
				}
			}

			if (SceneManagement.Instance.Multiplayer)
            {
				if (player2Info.player == null)
				{
					player2Info = new PlayerInfo(1, player2);
					player2Info.actualSide = Player.Side.Right;
					SetPosicion(player2Info);
				}
			}

			if (player1Info.player == null)
			{
				player1Info = new PlayerInfo(0, player1);
				player1Info.actualSide = SceneManagement.Instance.Multiplayer ? Player.Side.Left : Player.Side.Center;

				SetPosicion(player1Info);
			}

			if (player1Info.player != null && player2Info.player != null && SceneManagement.Instance.Multiplayer)
			{
				if(player1Info.endedTutorial2 && player2Info.endedTutorial2)
				{
					StartRace();
				}
			}
			else if (player1Info.player != null && !SceneManagement.Instance.Multiplayer)
            {
				if (player1Info.endedTutorial2)
				{
					StartRace();
				}
			}

			break;

		case GameState.Playing:
			
			//SKIP LA CARRERA
			if(Input.GetKey(KeyCode.Mouse1) && 
			   Input.GetKey(KeyCode.Keypad0))
			{
				gameTime = 0;
			}
			
			if(gameTime <= 0)
			{
				EndRace();
			}
			
			if(countingBack)
			{				
				countToStart -= T.GetDT();
				if(countToStart < 0)
				{
					StartRace();
					countingBack = false;
				}
			}
			else
			{
				//baja el tiempo del juego
				gameTime -= T.GetDT();
				if(gameTime <= 0)
				{
					//termina el juego
				}
			}
			
			break;
			
			
		case GameState.Ended:
			
			//nada de trakeo con kinect, solo se muestra el puntaje
			//tambien se puede hacer alguna animacion, es el tiempo previo a la muestra de pts
			
			waitTimeForShowingPoints -= Time.deltaTime;
			if(waitTimeForShowingPoints <= 0)
			{
				SceneManagement.Instance.GoToFinalScene();
					//Application.LoadLevel(Application.loadedLevel +1);		
			}		
			
			break;		
		}
	}

	public void DeactivatePlayer2()
    {
		player2.TurnOff();
		player2.gameObject.SetActive(false);
	}

	public void InitializeCalibration()
	{
		for(int i = 0; i < ObjsCalibracion1.Length; i++)
		{
			ObjsCalibracion1[i].SetActiveRecursively(true);

			if (SceneManagement.Instance.Multiplayer)
			{
				ObjsCalibracion2[i].SetActiveRecursively(true);
			}
		}

		for (int i = 0; i < ObjsTuto1.Length; i++)
		{
			ObjsTuto1[i].SetActiveRecursively(false);
			ObjsTuto2[i].SetActiveRecursively(false);
		}

		for(int i = 0; i < ObjsCarrera.Length; i++)
		{
			ObjsCarrera[i].SetActiveRecursively(false);
		}
		
		player1.ChangeToCalibration();

		if (SceneManagement.Instance.Multiplayer)
		{ 
			player2.ChangeToCalibration(); 
		}
	}
	
	void CambiarATutorial()
	{
		player1Info.calibrationEnded = true;
			
		for(int i = 0; i < ObjsTuto1.Length; i++)
		{
			ObjsTuto1[i].SetActiveRecursively(true);
		}
		
		for(int i = 0; i < ObjsCalibracion1.Length; i++)
		{
			//ObjsCalibracion1[i].SetActiveRecursively(false);
		}

		player1.GetComponent<BrakeManager>().Brake();
		player1.ChangeToTutorial();
		player1.gameObject.transform.position = PosCamion1Tuto;
		player1.transform.forward = Vector3.forward;

		for (int i = 0; i < ObjsCalibracion2.Length; i++)
		{
			ObjsCalibracion2[i].SetActiveRecursively(false);
		}

		if (SceneManagement.Instance.Multiplayer)
		{
			player2Info.calibrationEnded = true;

			for (int i = 0; i < ObjsTuto2.Length; i++)
			{
				ObjsTuto2[i].SetActiveRecursively(true);
			}
			player2.GetComponent<BrakeManager>().Brake();
			player2.gameObject.transform.position = PosCamion2Tuto;
			player2.ChangeToTutorial();
			player2.transform.forward = Vector3.forward;
		}
	}
	
	void StartRace()
	{
		player1.GetComponent<BrakeManager>().RestoreVelocity();
		player1.GetComponent<ControlDireccion>().enabledToMove = true;

		if (SceneManagement.Instance.Multiplayer)
		{
			player2.GetComponent<BrakeManager>().RestoreVelocity();
			player2.GetComponent<ControlDireccion>().enabledToMove = true;
		}
        else
        {
			player2DivingCam.UnableCamera();
		}
	}
	
	void EndRace()
	{		
		actualState = GameState.Ended;
		
		gameTime = 0;

		if (SceneManagement.Instance.Multiplayer)
		{
			if (player1.money > player2.money)
			{
				if (player1Info.actualSide == Player.Side.Right)
					DatosPartida.winningSide = DatosPartida.Side.Right;
				else
					DatosPartida.winningSide = DatosPartida.Side.Left;

				DatosPartida.winnerScore = player1.money;
				DatosPartida.loserScore = player2.money;
			}
			else
			{
				//lado que gano
				if (player2Info.actualSide == Player.Side.Right)
					DatosPartida.winningSide = DatosPartida.Side.Right;
				else
					DatosPartida.winningSide = DatosPartida.Side.Left;

				DatosPartida.winnerScore = player2.money;
				DatosPartida.loserScore = player1.money;
			}
		}
        else
        {
			DatosPartida.winningSide = DatosPartida.Side.Center;

			DatosPartida.winnerScore = player1.money;
			DatosPartida.loserScore = 0;
		}
		
		player1.GetComponent<BrakeManager>().Brake();
		player1.unloadController.EndOfTheGame();

		if (SceneManagement.Instance.Multiplayer)
		{
			player2.GetComponent<BrakeManager>().Brake();
			player2.unloadController.EndOfTheGame();
		}
	}
	
	void SetPosicion(PlayerInfo playerInfo)
	{
		playerInfo.player.SetSide(playerInfo.actualSide);
		playerInfo.player.calibrationController.InitTesting();

		if (SceneManagement.Instance.Multiplayer)
        {
			if (playerInfo.player == player1)
			{
				playerInfo.player.SetSide(Player.Side.Right);
			}
			else
			{
				playerInfo.player.SetSide(Player.Side.Left);
			}
		}
	}

	void ChangeToRace()
	{
		for (int i = 0; i < ObjsCarrera.Length; i++)
		{
			ObjsCarrera[i].SetActiveRecursively(true);
		}

		player1Info.calibrationEnded = true;

		for (int i = 0; i < ObjsTuto1.Length; i++)
		{
			ObjsTuto1[i].SetActiveRecursively(true);
		}

		for (int i = 0; i < ObjsCalibracion1.Length; i++)
		{
			ObjsCalibracion1[i].SetActiveRecursively(false);
		}

		int playerPosIndex = SceneManagement.Instance.Multiplayer ? 0 : 2;
		player1.gameObject.transform.position = truckRacePosition[playerPosIndex];

		player1.transform.forward = Vector3.forward;
		player1.GetComponent<BrakeManager>().Brake();
		player1.ChangeToConduction();

		player1.GetComponent<BrakeManager>().RestoreVelocity();
		player1.GetComponent<ControlDireccion>().enabledToMove = false;
		player1.transform.forward = Vector3.forward;

		if (SceneManagement.Instance.Multiplayer)
		{ 
			player2Info.calibrationEnded = true;

			for (int i = 0; i < ObjsCalibracion2.Length; i++)
			{
				ObjsCalibracion2[i].SetActiveRecursively(false);
			}

			for (int i = 0; i < ObjsTuto2.Length; i++)
			{
				ObjsTuto2[i].SetActiveRecursively(true);
			}

			player2.gameObject.transform.position = truckRacePosition[1];

			player2.transform.forward = Vector3.forward;
			player2.GetComponent<BrakeManager>().Brake();
			player2.ChangeToConduction();

			player2.GetComponent<BrakeManager>().RestoreVelocity();
			player2.GetComponent<ControlDireccion>().enabledToMove = false;
			player2.transform.forward = Vector3.forward;
		}
		
		actualState = GameState.Playing;
	}
	
	public void EndTutorial(int playerID)
	{
		if (playerID == 0)
		{
			player1Info.endedTutorial2 = true;
		}

		if (SceneManagement.Instance.Multiplayer)
		{
			if (playerID == 1)
			{
				player2Info.endedTutorial2 = true;
			}

			if (player1Info.endedTutorial2 && player2Info.endedTutorial2)
			{
				ChangeToRace();
			}
		}
        else
        {
			if (player1Info.endedTutorial2)
			{
				ChangeToRace();
			}
		}
	}
	
	public void EndCalibration(int playerID)
	{
		if (playerID == 0)
		{
			player1Info.endedTutorial1 = true;
		}

		if (SceneManagement.Instance.Multiplayer)
		{
			if (playerID == 1)
			{
				player2Info.endedTutorial1 = true;
			}

			if (player1Info.player != null && player2Info.player != null)
			{ 
				if (player1Info.endedTutorial1 && player2Info.endedTutorial1)
				{ 
					ChangeToRace();//CambiarATutorial();
				}
			}
		}
		else
        {
			if (player1Info.player != null)
			{
				if (player1Info.endedTutorial1)
				{
					ChangeToRace();//CambiarATutorial();
				}
			}
		}
	}
	
	[System.Serializable]
	public class PlayerInfo
	{
		public PlayerInfo(int inputType, Player player)
		{
            this.inputType = inputType;
			this.player = player;
		}
		
		public bool calibrationEnded = false;
		public bool endedTutorial1 = false;
		public bool endedTutorial2 = false;
		
		public Player.Side actualSide;

        public int inputType = -1;
		
		public Player player;
	}
}
