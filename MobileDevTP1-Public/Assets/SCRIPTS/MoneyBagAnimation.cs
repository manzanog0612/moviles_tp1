using UnityEngine;

public class MoneyBagAnimation : MonoBehaviour 
{
	private Vector3 initialPosition;
	private bool goingUp = true;
	private Vector3 angularAuxiliarVelocity = Vector3.zero;
	private float initTime;
	private bool initDone = false;

	[SerializeField] private float turnVelocity = 1;
	[SerializeField] private bool turn = true;
	[SerializeField] private bool verticalMovement = true;
	[SerializeField] private Vector3 amplitude = Vector3.zero;
	[SerializeField] private float movementVelocity = 1;

	private void Start ()
	{
		initialPosition = transform.position;
		
		initTime = Random.Range(0, 2);
	}

	private void Update ()
	{
		if (!initDone)
		{
			initTime -= Time.deltaTime;
			if (initTime <= 0)
				initDone = true;

			return;
		}

		if (turn)
		{
			angularAuxiliarVelocity = Vector3.zero;
			angularAuxiliarVelocity.y = T.GetDT() * turnVelocity;
			transform.localEulerAngles += angularAuxiliarVelocity;
		}
		
		if(verticalMovement)
		{
			Vector3 speed = amplitude.normalized * Time.deltaTime * movementVelocity;

			speed = goingUp ? speed : -speed;

			transform.localPosition += speed;

			if ((transform.position - initialPosition).magnitude > amplitude.magnitude / 2)
			{
				goingUp = !goingUp;
				transform.localPosition -= speed;
			}
		}
	}
}
