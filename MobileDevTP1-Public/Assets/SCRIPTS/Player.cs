using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour, ISubject
{
	public int money = 0;
	public int idPlayer = 0;
	public bool isOff = false;
	
	public MoneyBag[] bags = null;
	int actualBagsAmount = 0;
	public string bagTag = "";

	public enum Side { Left, Right, Center }
	public Side actualSide;

	public enum States {OnUnload, Driving, InCalibration, InTutorial, Off}
	public States actualState = States.Driving;
	
	public bool driving = true;
	public bool unloading = false;

	public UnloadController unloadController = null;
	public CalibrationController calibrationController = null;
	public ContrTutorial tutorialController = null;

	private VisualizationController visualization = null;

	private void Awake()
    {
		unloadController.Player = this;

		for (int i = 0; i < bags.Length; i++)
			bags[i] = null;

		visualization = GetComponent<VisualizationController>();
	}

	public bool AddBag(MoneyBag bag)
	{
		if(actualBagsAmount + 1 <= bags.Length)
		{
			bags[actualBagsAmount] = bag;
			actualBagsAmount++;
			money += (int)bag.ValueAmount;
			bag.Disapear();

			Notify();

			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void EmptyInventory()
	{
		if (isOff) return;

		for (int i = 0; i< bags.Length;i++)
			bags[i] = null;
		
		actualBagsAmount = 0;

		Notify();
	}
	
	public bool HasBags()
	{
		for(int i = 0; i< bags.Length;i++)
		{
			if(bags[i] != null)
			{
				return true;
			}
		}
		return false;
	}
	
	public void SetUnloadController(UnloadController controller)
	{
		unloadController = controller;
	}
	
	public UnloadController GetUnloadController()
	{
		return unloadController;
	}
	
	public void ChangeToCalibration()
	{
		if (isOff) return;
		actualState = States.InCalibration;
		Notify();
	}
	
	public void ChangeToTutorial()
	{
		if (isOff) return;
		actualState = States.InTutorial;
		tutorialController.Iniciar();
		Notify();
	}
	
	public void ChangeToConduction()
	{
		if (isOff) return;
		actualState = States.Driving;
		Notify();
	}
	
	public void ChangeToUnload()
	{
		if (isOff) return;
		actualState = States.OnUnload;
		Notify();
	}
	
	public void GetBagOut()
	{
		for(int i = 0; i < bags.Length; i++)
		{
			if(bags[i] != null)
			{
				bags[i] = null;
				Notify();
				return;
			}				
		}

	}

	public void TurnOff()
    {
		actualState = States.Off;
		isOff = true;
		Notify();

		for (int i = 0; i < observers.Count; i++)
		{
			Detach(observers[i]);
		}
	}

	public void SetSide(Side side)
    {
		if (isOff) return;
		actualSide = side;

		Notify();
	}

	#region ISubject
	private List<IObserver> observers = new List<IObserver>();

	public void Attach(IObserver observer)
	{
		observers.Add(observer);
	}

	public void Detach(IObserver observer)
	{
		observers.Remove(observer);
	}

	public void Notify()
	{
		foreach (var observer in observers)
		{
			observer.ObserverUpdate(this);
		}
	}

	public States GetState()
    {
		return actualState;
	}

	public Side GetSide()
	{
		return actualSide;
	}
	#endregion
}
