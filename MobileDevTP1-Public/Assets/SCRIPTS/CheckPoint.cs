using UnityEngine;

public class CheckPoint : MonoBehaviour
{
	private bool respawnEnabled = true;
	private float time = 0;

	[SerializeField] private string playerTag = "Player";
	[SerializeField] private float permanenceTime = 0.7f;//tiempo que no deja respaunear a un pj desp que el otro lo hizo.

	private void Start ()
	{
		GetComponent<Renderer>().enabled = false;
	}

	private void Update () 
	{
		if(!respawnEnabled)
		{
			time += T.GetDT();
			if(time >= permanenceTime)
			{
				time = 0;
				respawnEnabled = true;
			}
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == playerTag)
		{
			other.GetComponent<Respawn>().AddCP(this);
		}	
	}

	private void OnTriggerExit(Collider other)
	{
		if(other.tag == playerTag)
		{
			respawnEnabled = true;
		}
	}
	
	public bool IsEnabled()
	{
		if(respawnEnabled)
		{
			respawnEnabled = false;
			time = 0;
			return true;
		}
		else
		{
			return respawnEnabled;
		}
	}
}
