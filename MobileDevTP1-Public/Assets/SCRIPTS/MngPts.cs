using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class MngPts : MonoBehaviour 
{
	float Tempo = 0;
	
	int indexGanador = 0;

	[SerializeField] private GameObject player1Background = null;
	[SerializeField] private TextMeshProUGUI[] money = null;
	[SerializeField] private GameObject[] result = null;

	public float TiempEspReiniciar = 10;
	
	public bool ActivadoAnims = false;

	void Start () 
	{
		SetScene();
	}
	
	// Update is called once per frame
	void Update () 
	{		
		//CIERRA LA APLICACION
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
	
	void SetScene()
	{
        for (int i = 0; i < result.Length; i++)
        {
			result[i].SetActive(false);
		}

		player1Background.SetActive(false);

		if (SceneManagement.Instance.Multiplayer)
        {
			if (DatosPartida.winningSide == DatosPartida.Side.Left)//izquierda
			{
				money[0].text = "$" + DatosPartida.winnerScore.ToString();
				money[1].text = "$" + DatosPartida.loserScore.ToString();

				result[0].SetActive(true);
			}
			else
			{
				money[1].text = "$" + DatosPartida.winnerScore.ToString();
				money[0].text = "$" + DatosPartida.loserScore.ToString();

				result[1].SetActive(true);
			}
		}
        else
        {
			money[0].text = "$" + DatosPartida.winnerScore.ToString();
			player1Background.SetActive(true);
			result[0].SetActive(true);
		}
	}
	
	public void DesaparecerGUI()
	{
		ActivadoAnims = false;
		Tempo = -100;
	}
}
