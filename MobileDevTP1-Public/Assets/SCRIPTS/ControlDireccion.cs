using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;


public class ControlDireccion : MonoBehaviour 
{
	private TruckInputManager input = null;
	private PlayerInput playerInput = null;

	[SerializeField] private CarController carController = null;
	[SerializeField] private float turn = 0;
	[SerializeField] private bool isPlayerOne = true;

	public bool enabledToMove = true;

    public float Turn { get => turn; }
    public TruckInputManager Input => input; 

    private void Awake()
    {
		carController = GetComponent<CarController>();
		playerInput = GetComponent<PlayerInput>();

#if UNITY_STANDALONE
		input = new TruckInputKeys(isPlayerOne);
#elif UNITY_ANDROID || UNITY_IOS
		input = new TruckInputTouchs(playerInput);
#elif  UNITY_EDITOR
		input = new TruckInputKeys(isPlayerOne);
#endif

	}

	private void Update () 
	{
		if (enabledToMove)
		{
			carController.SetTurn(input.GetHorizontalAxis());
			carController.SetAcceleration(input.GetVerticalAxis());
		}
	}
}
