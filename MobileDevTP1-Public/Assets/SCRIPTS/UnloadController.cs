using UnityEngine;
using System.Collections;

public class UnloadController : MonoBehaviour 
{
	System.Collections.Generic.List<Pallet.Values> Ps = new System.Collections.Generic.List<Pallet.Values>();
	
	int Contador = 0;
	
	Deposit Dep;
	
	public GameObject[] Componentes;//todos los componentes que debe activar en esta escena
	
	private Player player = null;
	private Collider playerCollider = null;
	
	public Pallet PEnMov = null;
	
	//las camaras que enciende y apaga
	public GameObject CamaraConduccion;
	public GameObject CamaraDescarga;
	
	//los prefab de los pallets
	public GameObject Pallet1;
	public GameObject Pallet2;
	public GameObject Pallet3;
	
	public Estanteria Est1;
	public Estanteria Est2;
	public Estanteria Est3;
	
	public Cinta Cin2;
	
	public float Bonus = 0;
	float TempoBonus;
	
	
	public UnloadAnimationManager ObjAnimado;

    public Player Player { set => player = value; }

    void Start () 
	{
		for (int i = 0; i < Componentes.Length; i++)
		{
			Componentes[i].SetActiveRecursively(false);
		}
		
		playerCollider = player.GetComponent<Collider>();
		player.SetUnloadController(this);
		if(ObjAnimado != null)
			ObjAnimado.unloadController = this;
	}
	
	void Update () 
	{
		//contador de tiempo
		if(PEnMov != null)
		{
			//if(TempoBonus > 0)
			//{
			//	Bonus = TempoBonus * (int)PEnMov.value / PEnMov.time;
			//	TempoBonus -= T.GetDT();
			//}
			//else
			//{
			//	Bonus = 0;
			//}

			float multiplier = 1;

			if ((int)SceneManagement.Instance.GameMode != 2)
			{
				if ((int)SceneManagement.Instance.GameMode == 1 )
				{
					multiplier = 2;
				}
                else
                {
					multiplier = 0.5f;
				}
			}

			Bonus = PEnMov.GetBonus(ref TempoBonus) * multiplier;
		}		
	}
			
	public void Activar(Deposit d)
	{
		Dep = d;//recibe el deposito para que sepa cuando dejarlo ir al camion
		CamaraConduccion.SetActiveRecursively(false);//apaga la camara de conduccion
			
		//activa los componentes
		for (int i = 0; i < Componentes.Length; i++)
		{
			Componentes[i].SetActiveRecursively(true);
		}
		
		playerCollider.enabled = false;
		player.ChangeToUnload();
		
		GameObject go;
		//asigna los pallets a las estanterias
		for(int i = 0; i < player.bags.Length; i++)
		{
			if(player.bags[i] != null)
			{
				Contador++;
				
				switch(player.bags[i].ValueAmount)
				{
				case Pallet.Values.one:
					go = Instantiate(Pallet1);
					Est1.Receive(go.GetComponent<Pallet>());
					break;
					
				case Pallet.Values.two:
					go = Instantiate(Pallet2);
					Est2.Receive(go.GetComponent<Pallet>());
					break;
					
				case Pallet.Values.three:
					go = Instantiate(Pallet3);
					Est3.Receive(go.GetComponent<Pallet>());
					break;
				}
			}
		}
		//animacion
		ObjAnimado.Entrar();
	}
	
	//cuando sale de un estante
	public void SalidaPallet(Pallet p)
	{
		PEnMov = p;
		TempoBonus = p.time;
		player.GetBagOut();
		//inicia el contador de tiempo para el bonus
	}
	
	//cuando llega a la cinta
	public void SetPalletArrival(Pallet p)
	{
		//termina el contador y suma los pts
		
		//termina la descarga
		PEnMov = null;
		Contador--;
		
		player.money += (int)Bonus;
		
		if(Contador <= 0)
		{
			Finalizacion();
		}
		else
		{
			Est2.EncenderAnim();
		}
	}
	
	public void EndOfTheGame()
	{
		//metodo llamado por el GameManager para avisar que se termino el juego
		
		//desactiva lo que da y recibe las bolsas para que no halla mas flujo de estas
		Est2.enabled = false;
		Cin2.enabled = false;
	}
	
	void Finalizacion()
	{
		ObjAnimado.Salir();
	}
	
	public Pallet GetPalletEnMov()
	{
		return PEnMov;
	}
	
	public void FinAnimEntrada()
	{
		//avisa cuando termino la animacion para que prosiga el juego
		Est2.EncenderAnim();
	}
	
	public void FinAnimSalida()
	{
		//avisa cuando termino la animacion para que prosiga el juego
		
		for (int i = 0; i < Componentes.Length; i++)
		{
			Componentes[i].SetActiveRecursively(false);
		}
		
		CamaraConduccion.SetActiveRecursively(true);
		
		playerCollider.enabled = true;
		
		player.ChangeToConduction();
		
		Dep.LetGo();
	}
	
}
