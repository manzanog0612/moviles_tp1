﻿using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour 
{
    [SerializeField] private List<WheelCollider> throttleWheels = new List<WheelCollider>();
    [SerializeField] private List<WheelCollider> steeringWheels = new List<WheelCollider>();
    [SerializeField] private float throttleCoefficient = 20000f;
    [SerializeField] private float maxTurn = 20f;

    private float turn = 0f;
    private float acceleration = 1f;
	
	private void FixedUpdate () 
    {
        foreach (var wheel in throttleWheels) 
        {
            wheel.motorTorque = throttleCoefficient * T.GetFDT() * acceleration;
        }
        foreach (var wheel in steeringWheels) 
        {
            wheel.steerAngle = maxTurn * turn;
        }
        turn = 0f;
    }

    public void SetTurn(float turn) 
    {
        this.turn = turn;
    }

    public void SetAcceleration(float acceleration) 
    {
        this.acceleration = acceleration;
    }
}
